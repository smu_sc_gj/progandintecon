using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardColour
{
    NULL,
    GREEN,
    RED
}

public class Keycard : MyPickup
{
    [SerializeField]
    private CardColour colour = CardColour.NULL;

    public override void collect(MyPlayer player)
    {
        if(colour == CardColour.NULL)
            Debug.Log("Card colour is null");
        else
        {
            Keycards keys = player.GetComponent<Keycards>();
            keys.addKey(this);
        }

    }

    public CardColour getColour()
    {
        return colour;
    }
}