using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDoor : MyLock
{
    [SerializeField]
    private CardColour colour = CardColour.NULL;

    private Animation doorAnimation;

    new public void Awake()
    {
        base.Awake();
        doorAnimation = gameObject.GetComponent<Animation>();
    }

    public override bool tryLock(MyPlayer player)
    {
        Debug.Log("Trying the door");

        // Check he has the key
        Keycards keys = player.GetComponent<Keycards>();

        //Has he got the key?
        int keyIndex = keys.findKey(colour);

        // if index is -1 its not found
        if(keyIndex == -1)
        {
            Debug.Log("Player does not have key");
            return false;
        }
        else
        {
            // Open the door!
            doorAnimation.Play("DoorAnimation");

            // remove key?

            return true;
        }
        
    }
}