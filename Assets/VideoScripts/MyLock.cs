using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Contract for locks 

public interface IMyLock
{
    // All childred will have
    bool tryLock(MyPlayer player);
}

// Base class for pickups
// Other benefit - works in editor
// Abstract - can't make one 

public abstract class MyLock : MonoBehaviour, IMyLock
{
    // Delegate specifics
    public abstract bool tryLock(MyPlayer player);

    // Common stuff

    private bool unlocked;

    public void Awake()
    {
        unlocked = false;
    }

    void OnTriggerStay(Collider trigger)
    {
        // Am I unlocked
        if(unlocked == true) return;

        // Get player from collision
        GameObject go = trigger.gameObject;

        MyPlayer player = go.GetComponent<MyPlayer>();

        // if not player do nothing
        if(player == null)
            return;
        else
        {
            // if player call collect with player
            unlocked = this.tryLock(player);
        }

       
    }
}