using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keycards : MonoBehaviour
{
    [SerializeField]
    private List<Keycard> keys;

    public void Start()
    {
        keys = new List<Keycard>();
    }

    // Key operations 
    public void addKey(Keycard key)
    {
        keys.Add(key);
    }

    public int findKey(CardColour colour)
    {
        int keyIndex = -1;

        for(int i = 0; i < keys.Count; i++)
        {
            if(colour == keys[i].getColour())
            {
                keyIndex = i;
                return keyIndex;
            }
        }

        return keyIndex;
    }

}