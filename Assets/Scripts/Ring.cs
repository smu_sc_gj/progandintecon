using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A ring
public class Ring : MyPickup
{
    public override void collect(MyPlayer player)
    {
        player.addRing();
    }
}