﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCMovement: MonoBehaviour
{
    private CharacterController controller;

    private Vector3 velocity;
    [SerializeField] private float jumpMultiplier = 10.0f;
    [SerializeField] private AnimationCurve jumpFalloff;
    private bool isJumping = false;

    [SerializeField]
    private float speed = 10.0f;

    [SerializeField]
    private float gravity = -9.8f;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        jumpFalloff = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 move = new Vector3();

        if(controller.isGrounded)
            velocity.y = 0.0f;

        //Get player input. 
        move.x = Input.GetAxis("Horizontal");
        move.z = Input.GetAxis("Vertical");

        //Move character controller. 
        controller.Move(move * Time.deltaTime * speed);

        //Look where you are going
        if(move != Vector3.zero)
            transform.forward = move;

        // Apply gravity 
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && controller.isGrounded && !isJumping)
        {
            isJumping = true;
            StartCoroutine(JumpEvent());
        }
    }

    IEnumerator JumpEvent()
    {
        float timeInAir = 0.0f;

        do
        {
            float jumpForce = jumpFalloff.Evaluate(timeInAir);

            controller.Move(Vector3.up * jumpForce * jumpMultiplier * Time.deltaTime);
            timeInAir += Time.deltaTime;

            yield return null;

        } while (
            !controller.isGrounded &&
            controller.collisionFlags != CollisionFlags.Above
        );

        isJumping = false;
    }
}
