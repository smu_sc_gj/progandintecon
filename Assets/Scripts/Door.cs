using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Now let's implement a door type lock
public class Door : Lock
{
    private Animation doorAnimations;

    [SerializeField]
    private KeyType type = KeyType.NULL;

    new public void Awake()
    {
        base.Awake();
        doorAnimations = gameObject.GetComponent<Animation>();
       
    }

    public override bool tryLock(Player player)
    {
        //Debug.Log("Trying the door");

        Keyring keyring = player.GetComponent<Keyring>();


        int keyIndex = keyring.findKey(type);

        //Debug.Log("Key index is " + keyIndex);

        if( keyIndex !=-1) // -1 if not found. 
        {
            doorAnimations.Play("DoorAnimation");

            //if keys are consumed, remove the key
            keyring.removeKey(keyIndex);

            return true;
        }
        else
        {
            return false;
        }
        
    }
}