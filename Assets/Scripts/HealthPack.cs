using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Now let's implement a healthpack type pickup
public class HealthPack : Pickup
{
    [SerializeField]
    int healthGain = 100;

    public override void collect(Player player)
    {
        player.increaseHealth(healthGain);
    }
}