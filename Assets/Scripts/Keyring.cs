using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyring : MonoBehaviour
{
   
    [SerializeField]
    private List<Key> keys;

    public void Start()
    {
        keys = new List<Key>();
    }

    public void Update()
    {
       // Update UI?
    }

    public void addKey(Key key)
    {
        keys.Add(key);
    }

    public int findKey(Key key)
    {
        int keyIndex = -1;

        //Debug.Log("number of keys" + keys.Count);

        for (int i = 0; i < keys.Count; i++)
        {
            //Debug.Log("Key sought" + key.getKeyType());
            //Debug.Log("Key tested" + keys[i].getKeyType());

            if(key.getKeyType() == keys[i].getKeyType())
            {
                keyIndex = i;
                return keyIndex;
            }
                
        }

        // Didn't return early - key not found. 
        return keyIndex;
    }

    public int findKey(KeyType keytype)
    {
        int keyIndex = -1;

        //Debug.Log("number of keys" + keys.Count);

        for (int i = 0; i < keys.Count; i++)
        {
            //Debug.Log("Key sought" + keytype);
            //Debug.Log("Key tested" + keys[i].getKeyType());

            if(keytype == keys[i].getKeyType())
            {
                keyIndex = i;
                return keyIndex;
            }
                
        }

        // Didn't return early - key not found. 
        return keyIndex;
    }

    public bool removeKey(Key key)
    {
        int index = findKey(key);
        return removeKey(index);
    }

    public bool removeKey(int index)
    {
        if( index != 0)
        {
            keys.RemoveAt(index);
            return true;
        }
        else
        {
            return false;
        }
    }
}