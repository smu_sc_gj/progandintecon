using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyType
{
    NULL,
    RED,
    BLUE,
    YELLOW,
    GREEN
}

public class Key : Pickup
{
    [SerializeField]
    private KeyType type = KeyType.NULL;

    public override void collect(Player player)
    {
        // Do something when collected by the player
        if(type == KeyType.NULL)
            Debug.Log("Oh no, a NULL key");
        else
        {
            //get his keyring 
            Keyring keyring = player.GetComponent<Keyring>();
            keyring.addKey(this);
        }
        
    }

    public void setKeyType(KeyType keyType)
    {
        this.type = keyType;
    }

    public KeyType getKeyType()
    {
        return type;
    }


}