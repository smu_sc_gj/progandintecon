using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Now let's implement a coin type pickup
public class Coin : Pickup
{
    public override void collect(Player player)
    {
        player.pickupCoin();
    }
}