﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class FloorGenerator : MonoBehaviour
{
    //Floor setup
    [SerializeField]
    private int maxRows = 10;

    [SerializeField]
    private int maxCols = 10;

    [SerializeField]
    private float stepHeight = 1.0f;

    [SerializeField]
    private int startRow = 0;

    [SerializeField]
    private int startCol = 0;

    [SerializeField]
    private int stepCount = 5;

    [SerializeField]
    private int stepRowDir = 1;

    [SerializeField]
    private int stepColDir = 1;

    [SerializeField]
    private Material colour1 = null;

    [SerializeField]
    private Material colour2 = null;

    private GameObject[] tiles;

    private float tileSize = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        if(colour1 == null)
            colour1 = new Material(Shader.Find("Specular"));
        
        if(colour2 == null)
            colour2 = new Material(Shader.Find("Specular"));

        GameObject cube = (this.transform.Find("MaskingCube").gameObject);
        cube.GetComponent<Renderer>().enabled = false;

        makeBoard();

        makeSteps();
    }

    void makeBoard()
    {
        float xOffset;
        float zOffset;
        bool colourFlag;
        int tileIndex = 0;

        tiles = new GameObject[maxCols * maxRows];

        for (int i = -maxCols / 2; i < maxCols / 2; i++)
        {
            for (int j = -maxRows / 2; j < maxRows / 2; j++)
            {
                xOffset = i * tileSize;
                zOffset = j * tileSize;
                colourFlag = ((i + j) % 2) != 0 ? true : false;
                tiles[tileIndex] = makeTile(xOffset, 0.0f, zOffset, colourFlag);
                tileIndex++;
            }
        }
    }

    private GameObject makeTile(float x, float y, float z, bool colour)
    {
        GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tile.transform.localScale = new Vector3(tileSize, 1.0f, tileSize);
        tile.transform.position = new Vector3(x, y, z);
        tile.transform.parent = transform;

        Renderer rend = tile.GetComponent<Renderer>();

        if(colour)
            rend.material = colour1;
        else
            rend.material = colour2;

        return tile;
    }

    private void makeSteps()
    {
        float stepY;
       

        bool colourFlag;
        for(int i = 0; i < stepCount+1; i++)
        {
            int colOffset = (startCol - maxCols/2)-1;
            int rowOffset = (startRow - maxRows/2)-1;

            colOffset += (stepColDir * i);
            rowOffset += (stepRowDir * i);

            float xOffset = colOffset * tileSize;
            float zOffset = rowOffset * tileSize;

            colourFlag = (stepCount % 2) != 0 ? true : false;


            for (int j = 0; j < i; j++)
            {
                colourFlag = ((i+j) % 2) != 0 ? true : false;
                stepY = stepHeight + (j * stepHeight);
                makeStack(xOffset, stepY, zOffset, colourFlag, stepHeight);
                colourFlag = !colourFlag;
            }
        }
    }

    private GameObject makeStack(float x, float y, float z, bool colour, float height)
    {
        GameObject stack = GameObject.CreatePrimitive(PrimitiveType.Cube);
        stack.transform.localScale = new Vector3(tileSize, height, tileSize);
        stack.transform.position = new Vector3(x, y, z);
        stack.transform.parent = transform;

        Renderer rend = stack.GetComponent<Renderer>();

        if (colour)
            rend.material = colour1;
        else
            rend.material = colour2;

        return stack;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
