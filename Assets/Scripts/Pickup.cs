using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This defines the interface (abstract class) for Pickups
public interface IPickup
{
    // The derived class will use this 
    // instance of player to modify player
	void collect(Player player);
}

// Let's first declare an Pickup class
// this is a hack to get the children of pickup 
// displayed correctly in the editor
public abstract class Pickup : MonoBehaviour, IPickup
{
    public abstract void collect(Player player);

    private bool collected;

    public void Awake()
    {
        collected = false;
    }

    void OnTriggerEnter(Collider trigger)
    {
        if(collected) return;

        GameObject go = trigger.gameObject;
        
        Player player;
        player = go.GetComponent<Player>();

        if(player != null)
        {
            this.collect(player);
            collected = true;
            
            MeshRenderer render = gameObject.GetComponent<MeshRenderer>();
            render.enabled = false;
        }
            
    }

}