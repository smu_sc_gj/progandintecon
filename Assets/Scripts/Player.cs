using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    int coins;

    [SerializeField]
    int health;

    const int MAX_HEALTH = 100;

    public void Start()
    {
        coins = 0;
        health = 50;  //for testing
    }

    public void Update()
    {
       
    }

    public void pickupCoin()
    {
        coins++;
    }

    public void increaseHealth(int healthIncrease)
    {
        // Increase health
        health += healthIncrease;

        // Cap health at the maximum. 
        if(health > MAX_HEALTH)
            health = MAX_HEALTH;
    }
}